# To Do List App Terminal

## Description
What is a ToDo List? It’s a list of tasks you need to complete, or things that you want to do. Having a list of everything you need to do written down in one place means you shouldn’t forget anything important.

## Usage
Run this program in your terminal using node.js.

## Roadmap
I would like to implement a database and the UI.
