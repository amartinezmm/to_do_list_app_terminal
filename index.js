const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
let taskList = [];
let current_mode = '';
function printInstructions() {
    if (current_mode === 'add') {
        console.log("Press 'c' to enter checking mode");
    } else if (current_mode === 'check') {
        console.log("Press 'a' to enter add task mode");
    }
    console.log("Press 'q' to quit program");
}
function addTask(taskList, taskDescription) {
    taskList.push({done: false, description: taskDescription});
}
function printTaskList (taskList) {
    for (let i = 0; i < taskList.length; i++) {
        indexNumber = i + 1;
        if (taskList[i].done) {
            console.log('[x] ' + indexNumber + '. ' + taskList[i].description);
        } else {
            console.log('[ ] ' + indexNumber + '. ' + taskList[i].description);
        }  
    }
}
function checkAllDone (taskList) {
    for (let task of taskList) {
        if (!task.done) return false;
    }
    return true;
}
function checkToDos (taskList, index) {
    if (index >= 0 && index < taskList.length) {
        taskList[index].done = true;
    } else {
        console.log('ERROR: Invalid task number');
        modeMarkTasks(taskList);
    }      
}

function modeInsertTask(taskList) {
    rl.question('Add a new task: ', function(taskDesc) {
        current_mode = 'add';
        switch (taskDesc) {
            case 'c': //check done tasks
                modeMarkTasks(taskList);
                break;
            case 'q': //quit application
                console.log('Stopping program');
                rl.close();
                break;
            default:
                addTask(taskList, taskDesc);
                console.log('To do list:');
                printTaskList(taskList);
                printInstructions();
                modeInsertTask(taskList);      
        }  
    });
}
function modeMarkTasks(taskList) {
    printTaskList(taskList); 
    rl.question('What task have you completed? (1 - ' + taskList.length + ') ', function(taskNumber) {
        current_mode = 'check';
        switch (taskNumber) {
            case 'a':
                modeInsertTask(taskList);
                break;
            case 'q':
                console.log('Stopping program...');
                rl.close();
                break;
            default:
                checkToDos(taskList, taskNumber - 1);
                if (checkAllDone(taskList)) {
                    printTaskList(taskList); 
                    console.log("Great job! All tasks are finished");
                    rl.close();
                } else {
                    modeMarkTasks(taskList);
                }
            }
        })
    }
modeInsertTask(taskList);
